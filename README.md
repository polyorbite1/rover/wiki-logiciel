# WIKI-LOGICIEL

### Bienvenue dans la documentation officielle pour le développement logiciel dans PolyOrbite-Rover!

## Structure

Nous avons séparé la documentation en deux : une partie pour l'équipe Contrôle et une partie pour l'équipe Bras Robotique. Dans chacune, une liste de pages de documentation est proposée.

Vous n'avez qu'à aller voir la documentation qui concerne votre équipe :
- **[Contrôle](https://gitlab.com/polyorbite1/rover/wiki-logiciel/-/blob/main/CONTROLE.md)**
- **[Bras Robotique](https://gitlab.com/polyorbite1/rover/wiki-logiciel/-/blob/main/BRAS-ROBOTIQUE.md)**

> La documentation de chaque équipe consiste en un fichier rempli d'hyperliens qui vous mèneront vers des pages de texte, des tutoriels et des exemples de code.

## Prérequis

- Notions de programmation procédurale en Python (mettre lien vers tutoriels...)
