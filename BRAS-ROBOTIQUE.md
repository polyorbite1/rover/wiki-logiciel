# WIKI-BRAS-ROBOTIQUE

Les formations suivantes ont pour but de vous permettre de comprendre le code développé par l'équipe Bras Robotique.

## Sommaire

1. [Pourquoi utiliser ROS?](https://gitlab.com/polyorbite1/rover/wiki-logiciel/-/wikis/Pourquoi-utiliser-ROS%3F)
2. [Les concepts de ROS : topic, node, service, etc.](https://gitlab.com/polyorbite1/rover/wiki-logiciel/-/wikis/Termes-utilis%C3%A9s-dans-ROS)
3. Structure d'un paquet ROS
4. CMake
5. ....

6. Structure du code et repositories Git pour l'équipe Bras Robotique
7. ...

## Aide-mémoire (mettre lien ici vers un wiki "aide-mémoire")

## Exemples de code

* Subscriber-Publisher minimal node
* DEL-toggling on Raspberry Pi 4
* ...

## Installation des outils de développement spécifiques à l'équipe Bras Robotique

Liens vers tutoriel d'installation de Ubuntu...
