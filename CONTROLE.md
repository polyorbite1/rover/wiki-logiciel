# WIKI-CONTROLE

Les formations suivantes ont pour but de vous permettre de comprendre le code développé par l'équipe Contrôle.

## Liens rapides
[Contrôle autonome](https://gitlab.com/polyorbite1/rover/wiki-logiciel/-/wikis/Configurer-le-contr%C3%B4le-autonome-sur-Ubuntu)

## Sommaire
-  Comment allumer Rover? 
-  [Pourquoi utiliser ROS?](https://gitlab.com/polyorbite1/rover/wiki-logiciel/-/wikis/Pourquoi-utiliser-ROS%3F)
-  [Les concepts de ROS : topic, node, service, etc.](https://gitlab.com/polyorbite1/rover/wiki-logiciel/-/wikis/Termes-utilis%C3%A9s-dans-ROS)
-  Structure d'un paquet ROS
- Nos packages ros: 
-  CMake
-  ....
- 
-  Structure du code et repositories Git pour l'équipe Contrôle
-  ...


## Aide-mémoire (mettre lien ici vers un wiki "aide-mémoire")

## Exemples de code

* Subscriber-Publisher minimal node
* DEL-toggling on Jetson TX2
* ...

## Installation des outils de développement spécifiques à l'équipe Contrôle

Liens vers tutoriel d'installation de Ubuntu...
